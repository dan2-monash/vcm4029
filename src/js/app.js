import $ from 'jquery';
import 'bootstrap';
import Rellax from 'rellax';
import Chart from 'chart.js'
import './module/clicker';

var rellax = new Rellax('.rellax');

// SIDE MENU
const menuTrigger = $('.burger-menu');
const closeButton = $('.close-btn');

menuTrigger.on('click', function () {
    document.getElementById('sideNavBar').style.transition = 'transform 500ms ease-in-out';
    document.getElementById('sideNavBar').style.transform = 'translateX(0)';
});

closeButton.on('click', function () {
    document.getElementById('sideNavBar').style.transition = 'transform 500ms ease-in-out';
    document.getElementById('sideNavBar').style.transform = 'translateX(-100vw)';
});



//API VODID-19 DATA WORLD DATA
var fatality;
var settings = {
	"async": true,
	"crossDomain": true,
	"url": "https://covid-19-data.p.rapidapi.com/totals?format=json",
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "covid-19-data.p.rapidapi.com",
		"x-rapidapi-key": "3e2e63b849msh8fc44cfa5e1e0a3p1f11f1jsn0a98405b9037"
	}
}



function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

if ( document.URL.includes("index.html") || document.URL.endsWith("8080/")) {
  $.ajax(settings).done(function (response) {
    $('#confirmed-case').html(numberWithCommas(response[0].confirmed));
    $('#recovered-case').html(numberWithCommas(response[0].recovered));
    $('#death-case').html(numberWithCommas(response[0].deaths));

    fatality = response[0].deaths / response[0].confirmed;
    $('#fatality-case').html(parseFloat(fatality.toFixed(2)) + '%');
    //console.log(response[0].confirmed);
  });
} else {

  var countryName = window.location.href.match(/keyword=(.+)/)[1];
  $('#country').html(countryName.toUpperCase());

  var auSetting = {
  	"async": true,
  	"crossDomain": true,
  	"url": "https://covid-19-data.p.rapidapi.com/country?format=json&name=" + countryName,
  	"method": "GET",
  	"headers": {
  		"x-rapidapi-host": "covid-19-data.p.rapidapi.com",
  		"x-rapidapi-key": "3e2e63b849msh8fc44cfa5e1e0a3p1f11f1jsn0a98405b9037"
  	}
  }

  $.ajax(auSetting).done(function (response) {
    $('#au-confirmed-case').html(numberWithCommas(response[0].confirmed));
    $('#au-recovered-case').html(numberWithCommas(response[0].recovered));
    $('#au-death-case').html(numberWithCommas(response[0].deaths));

    fatality = response[0].deaths / response[0].confirmed;
    $('#au-fatality-case').html(parseFloat(fatality.toFixed(2)) + '%');
    console.log(response[0].confirmed);



    //CHART
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Confirmed', 'Recovered', 'Deaths', 'In-hospital/Isolation'],
            datasets: [{
                label: '# of Votes',
                data: [response[0].confirmed, response[0].recovered, response[0].deaths, response[0].confirmed - response[0].recovered - response[0].deaths ],
                backgroundColor: [
                    '#4485FD',
                    '#00A651',
                    '#B5001B',
                    '#FFB74C'
                ],
                borderWidth: 2
            }]
        },
    });
})}




window.$ = $;
window.jQuery = $;
